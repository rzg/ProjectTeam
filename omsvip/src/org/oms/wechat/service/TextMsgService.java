package org.oms.wechat.service;

/**
 * 文本消息处理核心类
 * @author sunlight
 *
 */
public class TextMsgService {
	public static String replyMsg(String content,String fromUserName,String toUserName){
		String respMessage = null;  
        //TODO文字消息回复处理
		//百度音乐搜索
		if (content.startsWith("歌曲")) {
			content=content.replace(":", "").replace("：", "");
			respMessage=BaiduMusicService.getMusicByInputMsg(content, fromUserName, toUserName);
		}
        return respMessage;
	} 
}
