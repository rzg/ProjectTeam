package org.oms.wechat.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.oms.wechat.msg.resp.Article;
import org.oms.wechat.msg.resp.NewsMessage;
import org.oms.wechat.util.MessageUtil;
import org.oms.wechat.util.QqFaceUtil;

/**
 * 图文消息处理类
 * 
 * @author sunlight
 *
 */
public class NewsMsgService {
	public static String replyMsg(String content, String fromUserName,
			String toUserName) {
		String respMessage = null;
		// 创建图文消息
		NewsMessage newsMessage = new NewsMessage();
		newsMessage.setToUserName(fromUserName);
		newsMessage.setFromUserName(toUserName);
		newsMessage.setCreateTime(new Date().getTime());
		newsMessage.setMsgType(MessageUtil.RESP_MESSAGE_TYPE_NEWS);
		newsMessage.setFuncFlag(0);

		List<Article> articleList = new ArrayList<Article>();
		// 单图文消息
		if ("1".equals(content) || "单图文消息".equals(content)) {
			Article article = new Article();
			article.setTitle("分享快乐，不能没有你！");
			article.setDescription("岁月是条奔流不息的河。多少美丽的日子，灿烂的回忆，都在这跳动的节奏中渐渐远去、隐逝。但总有些东西给我们留下深刻印象，引导我们前进。或一个人，或一句话，或一件事。");
			article.setPicUrl("http://b144.photo.store.qq.com/psb?/7d2f9b41-9ea5-4801-b22d-1abc75005ef4/0pY*635sfyFCpVFiYYScgRqbrGTPCaNG9GLgfZG7Og4!/m/dB1z4lVxFQAA&bo=cgSAAkAGhAMFCB4!&rf=photolist");
			article.setUrl("http://user.qzone.qq.com/82068619/main");
			articleList.add(article);
			// 设置图文消息个数
			newsMessage.setArticleCount(articleList.size());
			// 设置图文消息包含的图文集合
			newsMessage.setArticles(articleList);
			// 将图文消息对象转换成xml字符串
			respMessage = MessageUtil.newsMessageToXml(newsMessage);
		}
		// 单图文消息---不含图片
		else if ("2".equals(content) || "单图文消息不含图片".equals(content)) {
			Article article = new Article();
			article.setTitle("微信公众帐号开发单图文消息不含图片");
			// 图文消息中可以使用QQ表情、符号表情
			article.setDescription("Sunlight，"
					+ QqFaceUtil.emoji(0x1F6B9)
					+ "，记得高三那年，班里来了一位语文实习老师。当时同学们都在想，都高三了，怎么还会安排实习老师？当然，那个问题最终不告而终。只隐约记得那是一位个 头不是很高，经常扎着一个马尾，年龄比我们大不了多少的女孩。她代课时课余时间很喜欢和同学们聊天。因为彼此年龄差距不大，话题就特别多。");
			// 将图片置为空
			article.setPicUrl("");
			article.setUrl("http://user.qzone.qq.com/82068619/main");
			articleList.add(article);
			newsMessage.setArticleCount(articleList.size());
			newsMessage.setArticles(articleList);
			respMessage = MessageUtil.newsMessageToXml(newsMessage);
		}
		// 多图文消息
		else if ("3".equals(content) || "多图文消息".equals(content)) {
			Article article1 = new Article();
			article1.setTitle("用双手诠释生活，用微笑面对明天。");
			article1.setDescription("");
			article1.setPicUrl("http://mmbiz.qpic.cn/mmbiz/A4gQEOXKFrribloph0KFU8JbxzyaaYWuHpSYLp7QtpECwgwPtcGibcSXSqtkoYIRibunM98cSWZsfEc6CXSib62mnw/0");
			article1.setUrl("http://user.qzone.qq.com/82068619/main");

			Article article2 = new Article();
			article2.setTitle("用双手诠释生活，用微笑面对后天。");
			article2.setDescription("");
			article2.setPicUrl("http://mmbiz.qpic.cn/mmbiz/A4gQEOXKFrribloph0KFU8JbxzyaaYWuHJib4hvw2O9ON18TW8GCadxAxCpbyzmwNFUNt9gWcykYAibI15I1cwmCg/0");
			article2.setUrl("http://user.qzone.qq.com/82068619/main");

			Article article3 = new Article();
			article3.setTitle("用双手诠释生活，用微笑面对未来。");
			article3.setDescription("");
			article3.setPicUrl("http://mmbiz.qpic.cn/mmbiz/A4gQEOXKFrribloph0KFU8JbxzyaaYWuHpSYLp7QtpECwgwPtcGibcSXSqtkoYIRibunM98cSWZsfEc6CXSib62mnw/0");
			article2.setUrl("http://user.qzone.qq.com/82068619/main");

			articleList.add(article1);
			articleList.add(article2);
			articleList.add(article3);
			newsMessage.setArticleCount(articleList.size());
			newsMessage.setArticles(articleList);
			respMessage = MessageUtil.newsMessageToXml(newsMessage);
		}
		// 多图文消息---首条消息不含图片
		else if ("4".equals(content) || "多图文消息首条消息不含图片".equals(content)) {
			Article article1 = new Article();
			article1.setTitle("我知道时间长了我们会将她渐渐遗忘。");
			article1.setDescription("");
			// 将图片置为空
			article1.setPicUrl("");
			article1.setUrl("http://user.qzone.qq.com/82068619/main");

			Article article2 = new Article();
			article2.setTitle("生活总会充满不如意。就像曾经高考不是很理想我曾一度想放弃一样，但最终我走过来了。");
			article2.setDescription("");
			article2.setPicUrl("http://mmbiz.qpic.cn/mmbiz/A4gQEOXKFrribloph0KFU8JbxzyaaYWuHJib4hvw2O9ON18TW8GCadxAxCpbyzmwNFUNt9gWcykYAibI15I1cwmCg/0");
			article2.setUrl("http://user.qzone.qq.com/82068619/main");

			Article article3 = new Article();
			article3.setTitle("用双手诠释生活，用微笑面对明天。");
			article3.setDescription("");
			article3.setPicUrl("http://mmbiz.qpic.cn/mmbiz/A4gQEOXKFrribloph0KFU8JbxzyaaYWuH4n6Xxf4HFKicLUrhNZvfVo1GC8TEuclZtumia0ibPricRyFBmXMaqjAShw/0");
			article2.setUrl("http://user.qzone.qq.com/82068619/main");

			Article article4 = new Article();
			article4.setTitle("大家生活一派欣荣，那是回报双手的辛勤。");
			article4.setDescription("");
			article4.setPicUrl("http://mmbiz.qpic.cn/mmbiz/A4gQEOXKFrribloph0KFU8JbxzyaaYWuHpSYLp7QtpECwgwPtcGibcSXSqtkoYIRibunM98cSWZsfEc6CXSib62mnw/0");
			article2.setUrl("http://user.qzone.qq.com/82068619/main");

			articleList.add(article1);
			articleList.add(article2);
			articleList.add(article3);
			articleList.add(article4);
			newsMessage.setArticleCount(articleList.size());
			newsMessage.setArticles(articleList);
			respMessage = MessageUtil.newsMessageToXml(newsMessage);
		}
		// 多图文消息---最后一条消息不含图片
		else if ("5".equals(content) || "多图文消息最后一条消息不含图片".equals(content)) {
			Article article1 = new Article();
			article1.setTitle("我发现我们如果一直抱怨生活，就会失去满手在握的美好明天和感受生活循序渐进的过程。");
			article1.setDescription("");
			article1.setPicUrl("http://mmbiz.qpic.cn/mmbiz/A4gQEOXKFrribloph0KFU8JbxzyaaYWuHpSYLp7QtpECwgwPtcGibcSXSqtkoYIRibunM98cSWZsfEc6CXSib62mnw/0");
			article1.setUrl("http://user.qzone.qq.com/82068619/main");

			Article article2 = new Article();
			article2.setTitle("只要面上一直挂着真诚的微笑，舞动双手，坚定向前，美好的明天就会展现在眼前。");
			article2.setDescription("");
			article2.setPicUrl("http://mmbiz.qpic.cn/mmbiz/A4gQEOXKFrribloph0KFU8JbxzyaaYWuH4n6Xxf4HFKicLUrhNZvfVo1GC8TEuclZtumia0ibPricRyFBmXMaqjAShw/0");
			article2.setUrl("http://user.qzone.qq.com/82068619/main");

			Article article3 = new Article();
			article3.setTitle("最后特别感谢柳峰！");
			article3.setDescription("");
			// 将图片置为空
			article3.setPicUrl("");
			article3.setUrl("http://blog.csdn.net/lyq8479");

			articleList.add(article1);
			articleList.add(article2);
			articleList.add(article3);
			newsMessage.setArticleCount(articleList.size());
			newsMessage.setArticles(articleList);
			respMessage = MessageUtil.newsMessageToXml(newsMessage);
		} else {
			respMessage = null;
		}
		return respMessage;
	}

	public static String clickMenuNews(String fromUserName, String toUserName) {
		// 创建图文消息
		NewsMessage newsMessage = new NewsMessage();
		newsMessage.setToUserName(fromUserName);
		newsMessage.setFromUserName(toUserName);
		newsMessage.setCreateTime(new Date().getTime());
		newsMessage.setMsgType(MessageUtil.RESP_MESSAGE_TYPE_NEWS);
		newsMessage.setFuncFlag(0);

		List<Article> articleList = new ArrayList<Article>();
		// 单图文消息
		Article article = new Article();
		article.setTitle("分享快乐，不能没有你！");
		article.setDescription("岁月是条奔流不息的河。多少美丽的日子，灿烂的回忆，都在这跳动的节奏中渐渐远去、隐逝。但总有些东西给我们留下深刻印象，引导我们前进。或一个人，或一句话，或一件事。");
		article.setPicUrl("http://b144.photo.store.qq.com/psb?/7d2f9b41-9ea5-4801-b22d-1abc75005ef4/0pY*635sfyFCpVFiYYScgRqbrGTPCaNG9GLgfZG7Og4!/m/dB1z4lVxFQAA&bo=cgSAAkAGhAMFCB4!&rf=photolist");
		article.setUrl("http://omsvip.sinaapp.com");
		articleList.add(article);
		// 设置图文消息个数
		newsMessage.setArticleCount(articleList.size());
		// 设置图文消息包含的图文集合
		newsMessage.setArticles(articleList);
		// 将图文消息对象转换成xml字符串
		return MessageUtil.newsMessageToXml(newsMessage);
	}
}
