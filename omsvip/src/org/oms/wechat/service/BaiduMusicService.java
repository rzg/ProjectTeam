package org.oms.wechat.service;

import java.io.InputStream;
import java.util.Date;
import java.util.List;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.oms.wechat.msg.resp.Music;
import org.oms.wechat.msg.resp.MusicMessage;
import org.oms.wechat.msg.resp.TextMessage;
import org.oms.wechat.util.HttpRequestUtil;
import org.oms.wechat.util.MessageUtil;
import org.oms.wechat.util.UrlUtil;

/**
 * 百度音乐搜索API操作类
 * @author sunlight
 *
 */
public class BaiduMusicService {
	
	public static String getMusicByInputMsg(String content,String fromUserName,String toUserName){
		// 回复文本消息
		TextMessage textMessage = new TextMessage();
		textMessage.setToUserName(fromUserName);
		textMessage.setFromUserName(toUserName);
		textMessage.setCreateTime(new Date().getTime());
		textMessage.setMsgType(MessageUtil.RESP_MESSAGE_TYPE_TEXT);
		textMessage.setFuncFlag(0);
		 // 返回给微信服务器的xml消息  
        String respXml = null;  
		// 将歌曲2个字及歌曲后面的+、空格、-等特殊符号去掉  
        String keyWord = content.replaceAll("^歌曲[\\+ ~!@#%^-_=]?", "");  
        // 如果歌曲名称为空  
        if ("".equals(keyWord)) {  
        	respXml = getUsage();
        	textMessage.setContent(respXml);
    		return MessageUtil.textMessageToXml(textMessage);
        } else {  
            String[] kwArr = keyWord.split("@");  
            // 歌曲名称  
            String musicTitle = kwArr[0];  
            // 演唱者默认为空  
            String musicAuthor = "";  
            if (2 == kwArr.length)  
                musicAuthor = kwArr[1];  

            // 搜索音乐  
            Music music = BaiduMusicService.searchMusic(musicTitle, musicAuthor);  
            // 未搜索到音乐  
            if (null == music) {  
            	respXml = "对不起，没有找到你想听的歌曲<" + musicTitle + ">。";  
            	textMessage.setContent(respXml);
        		return MessageUtil.textMessageToXml(textMessage);
            } else {  
                // 音乐消息  
                MusicMessage musicMessage = new MusicMessage();  
                musicMessage.setToUserName(fromUserName);  
                musicMessage.setFromUserName(toUserName);  
                musicMessage.setCreateTime(new Date().getTime());  
                musicMessage.setMsgType(MessageUtil.RESP_MESSAGE_TYPE_MUSIC);  
                musicMessage.setMusic(music);  
                respXml = MessageUtil.musicMessageToXml(musicMessage);  
            }  
        }  
        return respXml;
	}
	
	/** 
     * 歌曲点播使用指南 
     *  
     * @return 
     */  
    public static String getUsage() {  
    	//歌曲点播操作指南\n回复：歌曲:+歌名\n例如：歌曲:存在\n或者：歌曲:存在@汪峰\n回复“歌曲”显示主菜单\n此处分号可以省略！
        StringBuffer buffer = new StringBuffer();  
        buffer.append("歌曲点播操作指南").append("\n\n");  
        buffer.append("回复：歌曲:+歌名").append("\n");  
        buffer.append("例如：歌曲:救赎之旅").append("\n");  
        buffer.append("或者：歌曲:救赎之旅@许巍").append("\n\n");  
        buffer.append("回复“歌曲”显示主菜单，此处分号可以省略！");  
        return buffer.toString();  
    }  
	
	/** 
     * 根据名称和作者搜索音乐 
     *  
     * @param musicTitle 音乐名称 
     * @param musicAuthor 音乐作者 
     * @return Music 
     */  
    public static Music searchMusic(String musicTitle, String musicAuthor) {  
    	
    	System.out.println("musicTitle==="+musicTitle);
        // 百度音乐搜索地址  
        String requestUrl = "http://box.zhangmen.baidu.com/x?op=12&count=1&title={TITLE}$${AUTHOR}$$$$";  
        // 对音乐名称、作者进URL编码  
        requestUrl = requestUrl.replace("{TITLE}",UrlUtil.urlEncodeUTF8(musicTitle));  
        requestUrl = requestUrl.replace("{AUTHOR}", UrlUtil.urlEncodeUTF8(musicAuthor));  
        // 处理名称、作者中间的空格  
        requestUrl = requestUrl.replaceAll("\\+", "%20");  
  
        // 查询并获取返回结果  
        InputStream inputStream = HttpRequestUtil.httpRequest2Stream(requestUrl);  
        // 从返回结果中解析出Music  
        Music music = parseMusic(inputStream);  
  
        // 如果music不为null，设置标题和描述  
        if (null != music) {  
            music.setTitle(musicTitle);  
            // 如果作者不为""，将描述设置为作者  
            if (!"".equals(musicAuthor))  
                music.setDescription(musicAuthor);  
            else  
                music.setDescription("来自百度音乐");  
        }  
        return music;  
    }  
    
    /** 
     * 解析音乐参数 
     *  
     * @param inputStream 百度音乐搜索API返回的输入流 
     * @return Music 
     */  
    @SuppressWarnings("unchecked")  
    private static Music parseMusic(InputStream inputStream) {  
        Music music = null;  
        try {  
            // 使用dom4j解析xml字符串  
            SAXReader reader = new SAXReader();  
            Document document = reader.read(inputStream);  
            // 得到xml根元素  
            Element root = document.getRootElement();  
            // count表示搜索到的歌曲数  
            String count = root.element("count").getText();  
            // 当搜索到的歌曲数大于0时  
            if (!"0".equals(count)) {  
                // 普通品质  
                List<Element> urlList = root.elements("url");  
                // 高品质  
                List<Element> durlList = root.elements("durl");  
  
                // 普通品质的encode、decode  
                String urlEncode = urlList.get(0).element("encode").getText();  
                String urlDecode = urlList.get(0).element("decode").getText();  
                // 普通品质音乐的URL  
                String url = urlEncode.substring(0, urlEncode.lastIndexOf("/") + 1) + urlDecode;  
                if (-1 != urlDecode.lastIndexOf("&"))  
                    url = urlEncode.substring(0, urlEncode.lastIndexOf("/") + 1) + urlDecode.substring(0, urlDecode.lastIndexOf("&"));  
  
                // 默认情况下，高音质音乐的URL 等于 普通品质音乐的URL  
                String durl = url;  
  
                // 判断高品质节点是否存在  
                Element durlElement = durlList.get(0).element("encode");  
                if (null != durlElement) {  
                    // 高品质的encode、decode  
                    String durlEncode = durlList.get(0).element("encode").getText();  
                    String durlDecode = durlList.get(0).element("decode").getText();  
                    // 高品质音乐的URL  
                    durl = durlEncode.substring(0, durlEncode.lastIndexOf("/") + 1) + durlDecode;  
                    if (-1 != durlDecode.lastIndexOf("&"))  
                        durl = durlEncode.substring(0, durlEncode.lastIndexOf("/") + 1) + durlDecode.substring(0, durlDecode.lastIndexOf("&"));  
                }  
                music = new Music();  
                // 设置普通品质音乐链接  
                music.setMusicUrl(url);  
                // 设置高品质音乐链接  
                music.setHQMusicUrl(durl);  
            }  
        } catch (Exception e) {  
            e.printStackTrace();  
        }  
        return music;  
    }  
  
    // 测试方法  
    public static void main(String[] args) {  
    	Music music = searchMusic("相信自己", "零点乐队");  
        System.out.println("音乐名称：" + music.getTitle());  
        System.out.println("音乐描述：" + music.getDescription());  
        System.out.println("普通品质链接：" + music.getMusicUrl());  
        System.out.println("高品质链接：" + music.getHQMusicUrl());
        
        //System.err.print(getMusicByInputMsg("歌曲：救赎之旅","22","333"));
        System.out.println(happyMusic("111","222"));
    } 
    
    public static String happyMusic(String fromUserName,String toUserName) {
    	return getMusicByInputMsg("歌曲救赎之旅", fromUserName, toUserName);
	}
}
