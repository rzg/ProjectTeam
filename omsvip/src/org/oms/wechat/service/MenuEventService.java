package org.oms.wechat.service;

import java.util.Date;

import org.oms.wechat.msg.resp.TextMessage;
import org.oms.wechat.util.MessageUtil;

/**
 * 菜单事件响应
 * 
 * @author sunlight
 *
 */
public class MenuEventService {
	public static String eventMenu(String eventKey, String fromUserName,
			String toUserName) {
		String respContent = null;
		TextMessage textMessage = new TextMessage();
		textMessage.setToUserName(fromUserName);
		textMessage.setFromUserName(toUserName);
		textMessage.setCreateTime(new Date().getTime());
		textMessage.setMsgType(MessageUtil.RESP_MESSAGE_TYPE_TEXT);
		textMessage.setFuncFlag(0);

		if (eventKey.equals("11")) {
			respContent = "天气预报菜单项被点击！";
		} else if (eventKey.equals("12")) {
			respContent = "公交查询菜单项被点击！";
		} else if (eventKey.equals("13")) {
			respContent = "周边搜索菜单项被点击！";
		} else if (eventKey.equals("14")) {
			respContent = "历史上的今天菜单项被点击！";
			textMessage.setContent(ExtractHtml.getTodayInHistoryInfo());
		} else if (eventKey.equals("20")) {
			return BaiduMusicService.happyMusic(fromUserName, toUserName);
		} else if (eventKey.equals("21")) {
			respContent = "歌曲点播操作指南\n回复：歌曲:+歌名\n例如：歌曲:救赎之旅\n或者：歌曲:救赎之旅@许巍\n回复“歌曲”显示主菜单\n此处分号可以省略！";
		} else if (eventKey.equals("22")) {
			respContent = "经典游戏菜单项被点击！";
		} else if (eventKey.equals("23")) {
			respContent = "美女电台菜单项被点击！";
		} else if (eventKey.equals("24")) {
			return NewsMsgService.clickMenuNews(fromUserName, toUserName);
		} else if (eventKey.equals("25")) {
			respContent = "聊天唠嗑菜单项被点击！";
		} else if (eventKey.equals("31")) {
			respContent = "手机号码归属地功能实现中...敬请期待！";
		} else if (eventKey.equals("32")) {
			respContent = "电影排行榜菜单项被点击！";
		} else if (eventKey.equals("33")) {
			respContent = "幽默笑话菜单项被点击！";
		} else if (eventKey.equals("30")) {
			respContent = "谢谢您的关注！\n<a href=\"http://www.65101.cn\">Sunlight的空间</a>"
					+ "\n回复1：单图文消息\n回复2：单图文消息不含图片\n回复3：多图文消息\n回复4：多图文消息首条消息不含图片\n回复5：多图文消息最后一条消息不含图片\n"
					+ "回复：“歌曲:+歌名”享受音乐带来的乐趣！";
		}
		if (textMessage.getContent() == null) {
			textMessage.setContent(respContent);
		}
		return MessageUtil.textMessageToXml(textMessage);
	}
}
