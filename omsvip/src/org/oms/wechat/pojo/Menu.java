package org.oms.wechat.pojo;
/**
 * 菜单 
 * @author sunlight
 *
 */
public class Menu {
	private Button[] button;

	public Button[] getButton() {
		return button;
	}

	public void setButton(Button[] button) {
		this.button = button;
	}
}
