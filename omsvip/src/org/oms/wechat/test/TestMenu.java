package org.oms.wechat.test;

import org.oms.wechat.pojo.AccessToken;
import org.oms.wechat.pojo.Button;
import org.oms.wechat.pojo.CommonButton;
import org.oms.wechat.pojo.ComplexButton;
import org.oms.wechat.pojo.Menu;
import org.oms.wechat.pojo.ViewButton;
import org.oms.wechat.util.WechatAccessToken;
import org.oms.wechat.util.WechatMenu;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TestMenu {
	private static Logger log = LoggerFactory.getLogger(TestMenu.class);  
	  
    public static void main(String[] args) {  
        // 第三方用户唯一凭证  
        String appId = "wx002bc67a6fe3e5a1";  
        // 第三方用户唯一凭证密钥  
        String appSecret = "39fd9f3452b76cc28062ec987f6efdfb";  
  
        // 调用接口获取access_token  
        AccessToken at = WechatAccessToken.getAccessToken(appId, appSecret);  
  
        if (null != at) {  
            // 调用接口创建菜单  
            int result = WechatMenu.createMenu(getMenu(), at.getToken());  
  
            // 判断菜单创建结果  
            if (0 == result)  
                log.info("菜单创建成功！");  
            else  
                log.info("菜单创建失败，错误码：" + result);  
        }  
    }  
  
    /** 
     * 组装菜单数据 
     *  
     * @return 
     */  
    private static Menu getMenu() {
    	ViewButton btn10 = new ViewButton();  
        btn10.setName("百度搜索");  
        btn10.setType("view");  
        btn10.setUrl("http://www.baidu.com");  
        
        CommonButton btn11 = new CommonButton();  
        btn11.setName("天气预报");  
        btn11.setType("click");  
        btn11.setKey("11");  
  
        CommonButton btn12 = new CommonButton();  
        btn12.setName("公交查询");  
        btn12.setType("click");  
        btn12.setKey("12");  
  
        CommonButton btn13 = new CommonButton();  
        btn13.setName("周边搜索");  
        btn13.setType("click");  
        btn13.setKey("13");  
  
        CommonButton btn14 = new CommonButton();  
        btn14.setName("历史上的今天");  
        btn14.setType("click");  
        btn14.setKey("14");  
        
        CommonButton btn20 = new CommonButton();  
        btn20.setName("救赎之旅");  
        btn20.setType("click");  
        btn20.setKey("20");  
        
        CommonButton btn21 = new CommonButton();  
        btn21.setName("歌曲点播");  
        btn21.setType("click");  
        btn21.setKey("21");  
  
        ViewButton btn22 = new ViewButton();  
        btn22.setName("我的空间");  
        btn22.setUrl("http://user.qzone.qq.com/82068619/main");
        btn22.setType("view");  
  
        CommonButton btn23 = new CommonButton();  
        btn23.setName("美女电台");  
        btn23.setType("click");  
        btn23.setKey("23");  
  
        CommonButton btn24 = new CommonButton();  
        btn24.setName("快乐驿站");  
        btn24.setType("click");  
        btn24.setKey("24");  
  
  
        CommonButton btn31 = new CommonButton();  
        btn31.setName("手机号码归属地");  
        btn31.setType("click");  
        btn31.setKey("31");  
  
        CommonButton btn32 = new CommonButton();  
        btn32.setName("电影排行榜");  
        btn32.setType("click");  
        btn32.setKey("32");  
  
        CommonButton btn33 = new CommonButton();  
        btn33.setName("幽默笑话");  
        btn33.setType("click");  
        btn33.setKey("33");  
        
        CommonButton btn30 = new CommonButton();  
        btn30.setName("使用帮助");  
        btn30.setType("click");  
        btn30.setKey("30"); 
  
        ComplexButton mainBtn1 = new ComplexButton();  
        mainBtn1.setName("生活助手");  
        mainBtn1.setSub_button(new Button[] { btn10, btn14 });  
  
        ComplexButton mainBtn2 = new ComplexButton();  
        mainBtn2.setName("休闲驿站");  
        mainBtn2.setSub_button(new Button[] { btn20,btn21,btn22,btn24 });  
  
        ComplexButton mainBtn3 = new ComplexButton();  
        mainBtn3.setName("更多体验");  
        mainBtn3.setSub_button(new Button[] { btn30,btn31});  
  
        /** 
         * 这是公众号xiaoqrobot目前的菜单结构，每个一级菜单都有二级菜单项<br> 
         *  
         * 在某个一级菜单下没有二级菜单的情况，menu该如何定义呢？<br> 
         * 比如，第三个一级菜单项不是“更多体验”，而直接是“幽默笑话”，那么menu应该这样定义：<br> 
         * menu.setButton(new Button[] { mainBtn1, mainBtn2, btn33 }); 
         */  
        Menu menu = new Menu();  
        menu.setButton(new Button[] { mainBtn1, mainBtn2, mainBtn3 });  
  
        return menu;  
    }  
}
