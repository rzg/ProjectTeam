<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Model And List</title>
</head>
<body>
	<h1>Welcome To SpringMVC</h1>
	<h2>this is Add User!需要引入JSTL And uri="http://www.springframework.org/tags/form"</h2>
	<sf:form method="post" modelAttribute="user">
		id===><sf:input path="id"/><br/>
		username===><sf:input path="username"/><br/>
		age===><sf:input path="age"/><br/>
		nickname===><sf:input path="nickname"/><br/>
		email===><sf:input path="email"/><br/>
		<input type="submit" value="添加用户">
	</sf:form>
</body>
</html>