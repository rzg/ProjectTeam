<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Model And List</title>
</head>
<body>
	<h1>Welcome To SpringMVC</h1>
	<h2>this is Add Person Validator!需要引入JSTL And uri="http://www.springframework.org/tags/form"</h2>
	<sf:form method="post" modelAttribute="person">
		id===><sf:input path="id"/><br/>
		name===><sf:input path="name"/><sf:errors path="name" cssStyle="color:red;"/><br/>
		age===><sf:input path="age"/><sf:errors path="age" cssStyle="color:red;"/><br/>
		email===><sf:input path="email"/><sf:errors path="email" cssStyle="color:red;"/><br/>
		birthday===><sf:input path="birthday"/><br/>
		<input type="submit" value="添加">
	</sf:form>
</body>
</html>