<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Model And List</title>
</head>
<body>
	<h1>Welcome To SpringMVC====>><a href="/SpringMVC">首页</a></h1>
	<h2>this is Request Model And List!需要引入JSTL ====>><a href="add.get">点击次出添加用户</a></h2>
	<h2>单个对象遍历</h2>
		<c:if test="${user!=null }">
		id:-----> ${user.id } <br/>
		username:-----> ${user.username } <br/>
		age:-----> ${user.age } <br/>
		nickname:-----> ${user.nickname } <br/>
		email:-----> ${user.email } <br/>
		</c:if>
	<hr/>
	
	<c:if test="${users!=null }">
	<h2>单纯的集合遍历</h2>
	<c:forEach items="${users }" var="user" varStatus="var">
		id:${user.id }
		username:${user.username }
		age:${user.age }
		nickname:${user.nickname }
		email: ${user.email } 
		<a href="del?index=${var.index}">删除</a>
		<hr width="600px" style="text-align: left;"/>
	</c:forEach>
	<h2>添加逻辑处理集合遍历</h2>
		<c:forEach items="${users }" var="user">
			<c:choose>
			<c:when test="${user==null }">
				User is null !<br/>
			</c:when>
			<c:otherwise>
				id:====>${user.id }
				username:====>${user.username }
				age:====>${user.age }
				nickname:====>${user.nickname }
				email:-----> ${user.email } <br/>
			</c:otherwise>
			</c:choose>
			
		</c:forEach>
		
		<h2>添加逻辑处理集合遍历</h2>
		<c:forEach items="${users }" var="user" varStatus="var">
			index:<c:out value="${var.index}"/>====>
			count:<c:out value="${var.count}"/>====>
			userName:<c:out value="${user.username}"/>
			email:-----> ${user.email } 
			<c:choose>
			<c:when test="${user==null }">
				====>User is null !<br/>
			</c:when>
			<c:otherwise>
				====>User is not null !<br/>
			</c:otherwise>
			</c:choose>
		</c:forEach>
	</c:if>
</body>
</html>