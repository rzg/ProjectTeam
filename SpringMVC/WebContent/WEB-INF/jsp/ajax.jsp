<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Welcome</title>
<script type="text/javascript" src="${pageContext.request.contextPath}/resource/js/jquery-1.10.1.js"></script>
<script type="text/javascript">
function ajax_click(){
	alert("进入方法....中文乱码");
	$.ajax({
			url: "printWriter.json",
			type: "get",
			dataType: "json",
			contentType:'application/json;charset=UTF-8',
			error: function() {
				alert("加载数据出错！");
			},
			success: function(data) {
				alert("成功！");
				alert(data[0].email);
				alert("evalVal====>>"+eval("('"+data[0]+"')"));
			}
		});
}

function ajax_list_click(){
	alert("进入方法....此处求一个最好用的方法遍历？");
	$.ajax({
			url: "printWriter.json",
			type: "get",
			dataType: "json",
			contentType:'application/json;charset=UTF-8',
			error: function() {
				alert("加载数据出错！");
			},
			success: function(data) {
				alert("成功！");
				for(var i=0;i<data.length;i++){
					alert("id="+data[i].id+"name="+data[i].name+"email="+data[i].email+"birthday="+data[i].birthday);
				}
			}
		});
}

function ajax_click_json(){
	alert("进入方法....不乱码");
	$.ajax({
			url: "person.json?json",
			data:{'index':2},
			type: "get",
			dataType: "json",
			contentType:'application/json;charset=UTF-8',
			error: function() {
				alert("加载数据出错！");
			},
			success: function(data) {
				alert("成功！");
				alert(data);
				alert(data.name+"===>email="+data.email+"birthday==>"+data.birthday);
				alert("evalVal====>>"+eval("('"+data.name+"')"));
			}
		});
}
</script>
</head>
<body>
	<h1>Welcome To SpringMVC ,this is Ajax to Json</h1>
	<button type="button" onclick="ajax_click();">点击按钮请求Ajax输出流方式</button><p/>
	<button type="button" onclick="ajax_list_click();">点击按钮请求Ajax输出流方式--集合遍历</button><p/>
	<button type="button" onclick="ajax_click_json();">点击按钮请求Ajax-json@ResponseBody</button>
	
	<hr/>
	<strong>注意：406 Not Acceptable<br/>指定的资源已经找到，但它的MIME类型和客户在Accpet头中所指定的不兼容</strong>
</body>
</html>