<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>SpringMVC</title>
<script type="text/javascript">
	function paramChartSet(){
		var val=encodeURI(encodeURI("我是中国人，你呢！"));
		location="param/code.map?username="+val;
	}
</script>
</head>
<body>
	<h1>SpringMVC From TO Spring Framework Reference</h1>
	<p><a href="welcome.shtml">1.非注解方式XML配置访问URL</a></p>
	<p><a href="person/person.shtml">2.访问URL(来自注解)load</a>&nbsp;And&nbsp;<a href="person/update.shtml">update</a>&nbsp;And&nbsp;<a href="person/delete.shtml">delete</a></p>
	<p><a href="param/str.map?username=hello，我是中国人">3.页面参数传递(@RequestParam参数必须传入的，否则报404错误，传值到页面用Map&lt;String, String&gt; context)</a></p>
	<p><a href="javascript:;" onclick="javascript:paramChartSet();">4.页面参数传递(@RequestParam参数必须传入的，否则报404错误，传值到页面用Map&lt;String, String&gt; context)，中文参数传递需要转码（encodeURI()）</a></p>
    <p><a href="param/str.model?username=I'm is paramter,哈哈">5.页面参数传递(传参是非必须的，没有参数则值为null，向页面传递值使用Model ---此方法长用)</a></p>
    <p><a href="user/user">6.Controller向页面传值（对象）</a>***<a href="user/model">model</a></p>
    <p><a href="user/users">7.Controller向页面传值（集合，使用-JSTL 标签遍历）</a>***<a href="user/users">list</a></p>
    <p><a href="person/add.shtml">8.Validator(添加日期字段会出现问题),添加了服务器端-字段验证</a> <strong>目前验证了空和邮箱，日期注解验证不了</strong></p>
    <p><a href="ajax/ajax.do">9.Ajax请求数据</a> **** <a href="ajax/person.json?index=2&json">9.1Ajax请求数据URL</a> **** <a href="ajax/person.json?index=2"> 9.2根据请求参数获取数据跳转到相应页面</a></p>
    <p><a href="ajax/printWriter.json">10.Ajax请求数据输出流方式(中文乱码)，此处注意406，produces = "text/html;charset=UTF-8"</a></p>
    
</body>
</html>