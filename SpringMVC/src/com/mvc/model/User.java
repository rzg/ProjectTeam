package com.mvc.model;

/**
 * User类，创建无参数及有参数的构造方法，重写toString()
 * 
 * @author Sunlight
 *
 */
public class User {
	public User() {

	}

	private int id;
	private String username;
	private int age;
	private String nickname;
	private String email;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public User(int id, String username, int age, String nickname, String email) {
		super();
		this.id = id;
		this.username = username;
		this.age = age;
		this.nickname = nickname;
		this.email = email;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", username=" + username + ", age=" + age
				+ ", nickname=" + nickname + ",email=" + email + "]";
	}

}
