package com.mvc.model;

import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.Range;

/**
 * 人类
 * 
 * @author Sunlight
 *
 */
public class Person {
	public Person() {

	}

	private int id;
	//@NotNull(message = "姓名不能为空！")
	@NotEmpty(message="姓名不能为空！")
	private String name;
	//@Size(min = 18, max = 100, message = "年龄范围在18~100之间！")
	@Range(min = 18, max = 100, message = "年龄范围在18~100之间！")
	private int age;
	@NotEmpty(message="邮箱不能为空！")
	@Email(message = "邮箱格式不正确！")
	private String email;
	
	//@Past(message = "生日应该小于今天！")
	private Date birthday;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public Person(int id, String name, int age, String email, Date birthday) {
		super();
		this.id = id;
		this.name = name;
		this.age = age;
		this.email = email;
		this.birthday = birthday;
	}

	@Override
	public String toString() {
		return "Person [id=" + id + ", name=" + name + ", age=" + age
				+ ", email=" + email + ", birthday=" + birthday + "]";
	}

}
