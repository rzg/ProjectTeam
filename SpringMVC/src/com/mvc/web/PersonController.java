package com.mvc.web;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mvc.model.Person;
/**
 * 注解方式打开链接
 * @author Sunlight
 *
 */
@Controller
public class PersonController {
	private List<Person> persons=new ArrayList<Person>();
	public PersonController(){
		persons.add(new Person(111,"我是中国第1人",22,"abc@123.com",new Date()));
		persons.add(new Person(111,"我是中国第2人",32,"abc@123.com",new Date()));
		persons.add(new Person(111,"我是中国第3人",42,"abc@123.com",new Date()));
		persons.add(new Person(111,"我是中国第4人",52,"abc@123.com",new Date()));
		persons.add(new Person(111,"我是中国第5人",62,"abc@123.com",new Date()));
		persons.add(new Person(111,"我是中国第6人",72,"abc@123.com",new Date()));
	}
	
	@RequestMapping(value={"/person/person.shtml","/person"})
	public String load(){
		System.out.println("Load Person!");
		return "person";
	}
	
	@RequestMapping(value={"/person/update.shtml","/person/update"})
	public String update(){
		System.out.println("Person is update!");
		return "person";
	}
	
	@RequestMapping(value={"/person/add.shtml","/person/add"},method=RequestMethod.GET)
	public String add(Model model){
		model.addAttribute(new Person());
		System.out.println("Person is add page!");
		return "add_person";
	}
	
	@RequestMapping(value={"/person/add.shtml","/person/add"},method=RequestMethod.POST)
	public String add(@Validated Person person,BindingResult br){
		System.out.println("Person is add!");
		if(br.hasErrors()){
			System.out.println("Person add error!");
			return "add_person";
		}
		persons.add(person);
		return "person";
	}
	
	@RequestMapping(value={"/person/delete.shtml","/person/delete"})
	public String delete(){
		System.out.println("Person is Delete!");
		return "person";
	}
}
