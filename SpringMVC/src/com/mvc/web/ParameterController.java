package com.mvc.web;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 页面参数传递
 * @author Sunlight
 *
 */
@Controller
@RequestMapping(value="/param")
public class ParameterController {
	/**
	 * 访问此url需要传入参数username，否则会404。
	 * @param username必须的参数
	 * @param context页面获取字参数
	 * @return
	 * @throws UnsupportedEncodingException 
	 */
	@RequestMapping("/code.map")
	public String getParam2Map(@RequestParam("username") String username,Map<String,String> context) throws UnsupportedEncodingException{
		username=URLDecoder.decode(username, "utf-8");
		System.out.println("username--map->>"+username);
		context.put("username", username);
		return "param";
	}
	
	@RequestMapping("/str.map")
	public String getParamCode2Map(@RequestParam("username") String username,Map<String,String> context) throws UnsupportedEncodingException{
		System.out.println("username--map->>"+username);
		context.put("username", username);
		return "param";
	}
	
	/**
	 * 访问此url可以传入参数username，也可不传，不传是为null。
	 * @param username非必须的参数
	 * @param context页面获取字参数
	 * @return
	 */
	@RequestMapping("/str.model")
	public String getParam2Model(String username,Model model){
		System.out.println("username--model->>"+username);
		model.addAttribute("username", username);
		return "param";
	}
}
