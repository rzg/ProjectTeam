package org.oms.service;

import java.util.List;

import org.oms.model.Sunlight;
import org.oms.model.pager.Pager;
/**
 * 
 * @author sunlight
 *
 */
public interface ISulightService extends IBaseService<Sunlight>{
	/**
	 * 此处添加在IBaseService没有的特殊接口
	 */
	/**
	 * 根据获取数据
	 * 
	 * @param hql
	 * @param args
	 * @return
	 */
	List<Sunlight> list(String hql, Object[] args);
	
	/**
	 * 获取数据
	 * 
	 * @param hql
	 * @param args
	 * @return
	 */
	List<Sunlight> list(String hql, Object args);

	/**
	 * 获取分页数据
	 * 
	 * @param hql
	 * @param args
	 * @return
	 */
	Pager<Sunlight> find(String hql, Object[] args);

	/**
	 * 获取分页数据
	 * 
	 * @param hql
	 * @param args
	 * @return
	 */
	Pager<Sunlight> find(String hql, Object args);

}
