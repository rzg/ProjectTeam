package org.oms.service.impl;

import java.util.List;

import org.oms.model.Sunlight;
import org.oms.model.pager.Pager;
import org.oms.service.BaseService;
import org.oms.service.ISulightService;
import org.springframework.stereotype.Service;

@Service("sunlightServiceImpl")
public class SunlightServiceImpl extends BaseService<Sunlight> implements ISulightService {
	/**
	 * 此处添加在BaseService没有的特殊方法
	 */
	@Override
	public List<Sunlight> list(String hql, Object[] args) {
		return this.dao.list(hql, args);
	}

	@Override
	public List<Sunlight> list(String hql, Object args) {
		return this.dao.list(hql, args);
	}

	@Override
	public Pager<Sunlight> find(String hql, Object[] args) {
		return this.dao.find(hql, args);
	}

	@Override
	public Pager<Sunlight> find(String hql, Object args) {
		return this.dao.find(hql, args);
	}
	
}
