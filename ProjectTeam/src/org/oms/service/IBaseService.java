package org.oms.service;

import java.util.List;

import org.oms.model.pager.Pager;

public interface IBaseService<T> {
	/**
	 * 新增
	 * 
	 * @param t
	 */
	void add(T t);

	/**
	 * 删除
	 * 
	 * @param id
	 */
	void delete(int id);

	/**
	 * 更新
	 * 
	 * @param t
	 */
	void update(T t);

	/**
	 * 获取一个对象
	 * 
	 * @param id
	 * @return
	 */
	T load(int id);


	/**
	 * 获取数据
	 * 
	 * @param hql
	 * @return
	 */
	List<T> listAll();


	/**
	 * 获取分页数据
	 * 
	 * @param hql
	 * @return
	 */
	Pager<T> findAll();
}
