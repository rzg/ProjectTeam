package org.oms.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.oms.model.pager.SystemContext;

public class SystemContextFilter implements Filter {
	private int pageSize = 10;

	@Override
	public void destroy() {

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		try {
			int pageOffset = 0;
			try {
				String pagerParam = request.getParameter("pager.offset");
				pageOffset = Integer.parseInt(pagerParam);
			} catch (NumberFormatException e) {

			}
			SystemContext.setPageOffset(pageOffset);
			SystemContext.setPageSize(pageSize);
			chain.doFilter(request, response);
		} finally {
			SystemContext.removePageOffset();
			SystemContext.removePageSize();
		}
	}

	@Override
	public void init(FilterConfig config) throws ServletException {
		try {
			pageSize = Integer.parseInt(config.getInitParameter("pageSize"));
		} catch (NumberFormatException e) {
			pageSize = 10;
		}
	}

}
