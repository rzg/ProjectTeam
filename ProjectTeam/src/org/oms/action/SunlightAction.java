package org.oms.action;

import java.util.Date;

import javax.annotation.Resource;

import org.oms.model.Sunlight;
import org.oms.service.ISulightService;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ModelDriven;

/**
 * 
 * @author sunlight
 *
 */
@SuppressWarnings("serial")
@Controller("sunlightAction")
@Scope("prototype")
public class SunlightAction extends BaseAction implements ModelDriven<Sunlight>{
	private Sunlight sunlight;
	private ISulightService sunlightServiceImpl;
	
	@Override
	public Sunlight getModel() {
		if (sunlight==null) {
			sunlight=new Sunlight();
		}
		return sunlight;
	}

	public Sunlight getSunlight() {
		return sunlight;
	}
	
	public void setSunlight(Sunlight sunlight) {
		this.sunlight = sunlight;
	}
	
	public ISulightService getSunlightServiceImpl() {
		return sunlightServiceImpl;
	}
	
	@Resource
	public void setSunlightServiceImpl(ISulightService sunlightServiceImpl) {
		this.sunlightServiceImpl = sunlightServiceImpl;
	}
	
	public String list(){
		ActionContext.getContext().put("sl", sunlightServiceImpl.findAll());
		return SUCCESS;
	}
	
	public String show(){
		sunlight=sunlightServiceImpl.load(sunlight.getSid());
		ActionContext.getContext().put("sunlight", sunlight);
		return SUCCESS;
	}
	
	public String delete(){
		sunlightServiceImpl.delete(sunlight.getSid());
		ActionContext.getContext().put("url", "/sunlight_list.action");
		return "redirect";
	}
	
	public String updateInput(){
		Sunlight s=sunlightServiceImpl.load(sunlight.getSid());
		sunlight.setName(s.getName());
		sunlight.setSummary(s.getSummary());
		return SUCCESS;
	}
	
	public String update(){
		Sunlight ts=sunlightServiceImpl.load(sunlight.getSid());
		ts.setName(sunlight.getName());
		ts.setSummary(sunlight.getSummary());
		sunlightServiceImpl.update(ts);
		ActionContext.getContext().put("url", "/sunlight_list.action");
		return "redirect";
	}
	
	public String addInput(){
		return SUCCESS;
	}
	
	public String add(){
		sunlight.setCreateDate(new Date());
		sunlightServiceImpl.add(sunlight);
		ActionContext.getContext().put("url", "/sunlight_list.action");
		return "redirect";
	}
	
	/**
	 * 服务端验证add的方法
	 */
	public void validateAdd() {
		if (sunlight.getName()==null || "".equals(sunlight.getName().trim())) {
			this.addFieldError("name", "姓名不能为空");
		}
	}
	
}
