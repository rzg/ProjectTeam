package org.oms.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
/**
 * Sunlight模板类
 * @author sunlight
 *
 */
@Entity
@Table(name="Sunlight")
public class Sunlight {
	private int sid;
	private String name;
	private String summary;
	private Date createDate;

	@Id
	@GeneratedValue
	public int getSid() {
		return sid;
	}

	public void setSid(int sid) {
		this.sid = sid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Sunlight() {
		super();
	}

	public Sunlight(String name, String summary, Date createDate) {
		super();
		this.name = name;
		this.summary = summary;
		this.createDate = createDate;
	}

	@Override
	public String toString() {
		return "Sunlight [sid=" + sid + ", name=" + name + ", summary="
				+ summary + ", createDate=" + createDate + "]";
	}

}
