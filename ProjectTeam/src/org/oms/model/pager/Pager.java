package org.oms.model.pager;

import java.util.List;

public class Pager<T> {
	/**
	 * 分页数据集
	 */
	private List<T> datas;
	/**
	 * 当前页
	 */
	private int pageOffset;
	/**
	 * 总条数
	 */
	private long totalRecord;
	/**
	 * 每页显示多少条
	 */
	private int pageSize;
	public List<T> getDatas() {
		return datas;
	}
	public void setDatas(List<T> datas) {
		this.datas = datas;
	}
	public int getPageOffset() {
		return pageOffset;
	}
	public void setPageOffset(int pageOffset) {
		this.pageOffset = pageOffset;
	}
	public long getTotalRecord() {
		return totalRecord;
	}
	public void setTotalRecord(long totalRecord) {
		this.totalRecord = totalRecord;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	
}
