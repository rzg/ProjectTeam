package org.oms.dao;

import java.util.List;

import org.oms.model.pager.Pager;
/**
 * Dao类模板接口
 * @author sunlight
 *
 * @param <T>
 */
public interface IBaseDao<T> {
	/**
	 * 新增
	 * 
	 * @param t
	 */
	void add(T t);

	/**
	 * 删除
	 * 
	 * @param id
	 */
	void delete(int id);

	/**
	 * 更新
	 * 
	 * @param t
	 */
	void update(T t);

	/**
	 * 获取一个对象
	 * 
	 * @param id
	 * @return
	 */
	T load(int id);

	/**
	 * 根据获取数据
	 * 
	 * @param hql
	 * @param args
	 * @return
	 */
	List<T> list(String hql, Object[] args);

	/**
	 * 获取数据
	 * 
	 * @param hql
	 * @return
	 */
	List<T> list(String hql);

	/**
	 * 获取数据
	 * 
	 * @param hql
	 * @param args
	 * @return
	 */
	List<T> list(String hql, Object args);

	/**
	 * 获取分页数据
	 * 
	 * @param hql
	 * @param args
	 * @return
	 */
	Pager<T> find(String hql, Object[] args);

	/**
	 * 获取分页数据
	 * 
	 * @param hql
	 * @param args
	 * @return
	 */
	Pager<T> find(String hql, Object args);

	/**
	 * 获取分页数据
	 * 
	 * @param hql
	 * @return
	 */
	Pager<T> find(String hql);
}
