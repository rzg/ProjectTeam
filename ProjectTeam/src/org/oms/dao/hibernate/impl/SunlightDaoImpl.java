package org.oms.dao.hibernate.impl;

import org.oms.dao.BaseDao;
import org.oms.dao.hibernate.ISulightDao;
import org.oms.model.Sunlight;
import org.springframework.stereotype.Repository;

@Repository("sunlightDaoImpl")
public class SunlightDaoImpl extends BaseDao<Sunlight> implements ISulightDao{
	/**
	 * 此处添加在BaseDao没有的特殊方法
	 */
}
