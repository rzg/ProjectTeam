package org.oms.dao.hibernate;

import org.oms.dao.IBaseDao;
import org.oms.model.Sunlight;

public interface ISulightDao extends IBaseDao<Sunlight> {
	/**
	 * 此处添加在IBaseDao没有的特殊接口
	 */
}
