<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="s" uri="/struts-tags"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Sunlight列表</title>
</head>
<body style="background: url('./source/images/bg/list_bg.jpg') fixed center center no-repeat;">
	<h3>
		Sunlight列表<a href="sunlight_addInput.action">添加Sunlight</a>
	</h3>
	SID ---- 姓名 ----------------------- 摘要 ------------------------------------ 创建时间<br/>
	<s:iterator value="#sl.datas">
	${sid} ----- <a href="sunlight_show.action?sid=${sid }">${name} </a>----${summary }------${createDate } 
	<a href="sunlight_delete.action?sid=${sid}">删除</a>|<a href="sunlight_updateInput.action?sid=${sid }">更新</a>
	<br /><br />
	</s:iterator>
	<br />
	<!-- 插入分页导航条 -->
	<jsp:include page="/including/pager.jsp">
		<jsp:param value="sunlight_list.action" name="url"/>
		<jsp:param value="${sl.totalRecord }" name="items"/>
	</jsp:include>
</body>
</html>