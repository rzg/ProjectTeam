<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="pg"  uri="http://jsptags.com/tags/navigation/pager" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<style type="text/css">
/* meneame style*/
div.pager-meneame {padding-right: 3px; padding-left: 3px; font-size: 80%; padding-bottom: 3px; margin: 3px; color: #ff6500; padding-top: 3px;; text-align: center}
div.pager-meneame a {border-right: #ff9600 1px solid; padding-right: 10px; background-position: 50% bottom; border-top: #ff9600 1px solid; padding-left: 10px; background-image: url(./source/images/pager/meneame.jpg); padding-bottom: 5px; border-left: #ff9600 1px solid; color: #ff6500; margin-right: 3px; padding-top: 5px; border-bottom: #ff9600 1px solid; text-decoration: none}
div.pager-meneame a:hover {border-right: #ff9600 1px solid; border-top: #ff9600 1px solid; background-image: none; border-left: #ff9600 1px solid; color: #ff6500; border-bottom: #ff9600 1px solid; background-color: #ffc794}
div.pager-meneame a:active {border-right: #ff9600 1px solid; border-top: #ff9600 1px solid; background-image: none; border-left: #ff9600 1px solid; color: #ff6500; border-bottom: #ff9600 1px solid; background-color: #ffc794}
div.pager-meneame font {border-right: #ff6500 1px solid; padding-right: 10px; border-top: #ff6500 1px solid; padding-left: 10px; font-weight: bold; padding-bottom: 5px; border-left: #ff6500 1px solid; color: #ff6500; margin-right: 3px; padding-top: 5px; border-bottom: #ff6500 1px solid; background-color: #ffbe94}
div.pager-meneame a.disabled {border-right: #ffe3c6 1px solid; padding-right: 10px; border-top: #ffe3c6 1px solid; padding-left: 10px; padding-bottom: 5px; border-left: #ffe3c6 1px solid; color: #ffe3c6; margin-right: 3px; padding-top: 5px; border-bottom: #ffe3c6 1px solid}
</style>
<div class="pager-meneame">
<pg:pager items="${param.items }" url="${param.url }" maxPageItems="10" export="currentPageNumber=pageNumber">  
    <pg:first>  
        <a href="${pageUrl }">&nbsp;首&nbsp;页&nbsp;</a>  
    </pg:first>  
    <pg:prev>  
        <a href="${pageUrl }">上一页</a>  
    </pg:prev>  
    <pg:pages>  
        <c:choose>  
            <c:when test="${currentPageNumber eq pageNumber}">  
                <font color="red">${pageNumber }</font>  
            </c:when>  
            <c:otherwise>  
                <a href="${pageUrl }">${pageNumber }</a>  
            </c:otherwise>  
        </c:choose>  
    </pg:pages>  
    <pg:next>  
        <a href="${pageUrl }">下一页</a>  
    </pg:next>  
    <pg:last>  
        <a href="${pageUrl }">&nbsp;尾&nbsp;页&nbsp;</a>  
    </pg:last>  
</pg:pager>  
</div>